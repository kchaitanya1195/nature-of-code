name := "nature-of-code"

version := "0.1"

scalaVersion := "2.13.2"

libraryDependencies ++= Seq(
  "org.processing" % "core" % "3.3.7",
  // "org.processing" % "net" % "3.0b5",        // Sending and receiving data over internet
  // "org.processing" % "video" % "3.0b5",      // Interacting with camera, playing and creating videos
  // "org.processing" % "serial" % "3.0b5",     // Interacting with external hardware (microcontrollers, etc.)
  // "org.processing" % "pde" % "3.0b5",        // Processing IDE
  // "org.processing" % "pdf" % "3.0b5"         // Interacting with pdf files
  // Check processing maven repo and official page for more libraries
)
