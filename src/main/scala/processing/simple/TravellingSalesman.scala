package processing.simple

import processing.CustomApplet
import processing.core.PVector

class TravellingSalesman extends CustomApplet {

  var cities: Array[PVector] = Array.empty

  var smallestDist: Float = 0
  var smallestPath: Array[Int] = Array.emptyIntArray

  var order: Array[Int] = Array.emptyIntArray

  override def init(): Unit = {
    order = (0 to 5).toArray
    cities = order.map(_ => new PVector(random(width), random(height)))
    smallestDist = distance()
    smallestPath = order.clone()
  }

  override def draw(): Unit = {
    background(0)

    fill(255)
    textSize(20)
    text(order.map(_.toString).reduce(_ + _), 20, 40)
    text(smallestDist.toString, 20, 70)

    fill(0xffff00ff)
    noStroke()
    cities.foreach(city => ellipse(city.x, city.y, 10, 10))

    noFill()
    stroke(255)
    strokeWeight(1)
    beginShape()
    order.foreach(i => vertex(cities(i).x, cities(i).y))
    endShape()

    strokeWeight(4)
    stroke(0xffff00ff)
    beginShape()
    smallestPath.foreach(i => vertex(cities(i).x, cities(i).y))
    endShape()

    val d = distance()
    if (d < smallestDist) {
      smallestDist = d
      smallestPath = order.clone()
    }

    nextLexOrder()
  }

  def drawArray(array: Array[PVector]): Unit = {
    beginShape()
    array.foreach(vector => vertex(vector.x, vector.y))
    endShape()
  }

  def swap[A](array: Array[A], i: Int, j: Int): Unit = {
    val temp = array(i)
    array(i) = array(j)
    array(j) = temp
  }

  def distance(): Float = {
    order.zip(order drop 1)
      .map(o => (cities(o._1), cities(o._2)))
      .map((PVector.dist _).tupled)
      .sum
  }

  def nextLexOrder(): Unit = {
    var x = order.zip(order drop 1).reverse.indexWhere(i => i._1 < i._2)
    if (x == -1) {
      noLoop()
      return
    }
    x = (order.length - 1) - x - 1

    var y = order.reverse.indexWhere(i => order(x) < i)
    y = (order.length - 1) - y

    swap(order, x, y)

    if (x < order.length - 2) {
      val (head, tail) = order.splitAt(x + 1)
      order = head.concat(tail.reverse)
    }
  }

}
