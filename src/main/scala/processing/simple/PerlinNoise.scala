package processing.simple

import processing.CustomApplet

class PerlinNoise extends CustomApplet {
  var t = 0f

  override def draw(): Unit = {
    t += 0.005f

    background(0)
    fill(255)

    val x = noise(t) * width

    ellipse(x, height / 2, 40, 40)
  }
}
