package processing.simple

import processing.CustomApplet
import processing.core.PApplet.constrain
import processing.core.PVector

class RandomWalker extends CustomApplet {
  var walker: Walker = _

  override def setup(): Unit = {
    background(255)
    walker = new Walker
  }

  override def draw(): Unit = {
    walker.step()
    walker.render()
  }

  class Walker {
    val STEP_SIZE = 1

    val RIGHT_CHANCE: Float = 0.25f
    val LEFT_CHANCE: Float = RIGHT_CHANCE + 0.25f
    val DOWN_CHANCE: Float = LEFT_CHANCE + 0.25f
    val UP_CHANCE: Float = DOWN_CHANCE + 0.25f

    val prevLoc = new PVector(0f, 0f)

    val location = new PVector(width / 2, height / 2)

    def step(): Unit = {
      prevLoc.x = location.x
      prevLoc.y = location.y

      random(1) match {
        case p if p < RIGHT_CHANCE => location.x += STEP_SIZE
        case p if p < LEFT_CHANCE => location.x -= STEP_SIZE
        case p if p < DOWN_CHANCE => location.y += STEP_SIZE
        case _ => location.y -= STEP_SIZE
      }

      location.x = constrain(location.x, 0, width - 1)
      location.y = constrain(location.y, 0, height - 1)
    }
    def render(): Unit = {
      stroke(0)
      strokeWeight(STEP_SIZE)
      line(prevLoc.x, prevLoc.y, location.x, location.y)
    }
  }
}
