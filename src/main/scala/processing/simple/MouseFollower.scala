package processing.simple

import processing.CustomApplet
import processing.core.PVector

class MouseFollower extends CustomApplet {
  var ball: Ball = _

  override def setup(): Unit = ball = new Ball

  override def draw(): Unit = {
    background(255)
    ball.update()
    ball.display()
  }

  class Ball {
    val location = new PVector(width / 2, height / 2)
    val velocity = new PVector(0, 0)

    def update(): Unit = {
      val mouse = new PVector(mouseX, mouseY)
      val acceleration = mouse.sub(location).mult(0.001f)

      velocity.add(acceleration)
      velocity.mult(0.99f)
      location.add(velocity)
    }

    def display(): Unit = {
      stroke(0)
      fill(50)

      ellipse(location.x, location.y, 40, 40)
    }
  }
}
