package processing.simple

import processing.CustomApplet
import processing.core.PVector

class PerlinWalker extends CustomApplet {
  var walker: Walker = _

  override def setup(): Unit = {
    background(255)
    walker = new Walker
  }

  override def draw(): Unit = {
    walker.step()
    walker.render()
  }

  class Walker {
    val prevPos = new PVector(0, 0)
    val pos = new PVector(0, 0)
    var noiseOffset = new PVector(random(1000), random(1000))

    private def getNoise = Array(noise(noiseOffset.x) * width, noise(noiseOffset.y) * height)

    def step(): Unit = {
      prevPos.set(getNoise)
      noiseOffset.add(0.01f, 0.01f)
      pos.set(getNoise)
    }
    def render(): Unit = {
      stroke(0)
      line(prevPos.x, prevPos.y, pos.x, pos.y)
    }
  }
}
