package processing.simple

import processing.CustomApplet
import processing.core.PVector

class Bouncing extends CustomApplet {
  var ball: Ball = _

  override def setup(): Unit = {
    ball = Ball(24)
  }
  override def draw(): Unit = {
    background(255)
    ball.move()
    ball.bounce()
    ball.display()
  }

  case class Ball(radius: Float) {
    val location = new PVector(width / 2, height / 2)
    val velocity = new PVector(random(2, 5), random(2, 5))

    def move(): Unit = location.add(velocity)

    def bounce(): Unit = {
      if (location.x > width - radius || location.x < 0 + radius)
        velocity.x = velocity.x * -1
      if (location.y > height - radius || location.y < 0 + radius)
        velocity.y = velocity.y * -1
    }

    def display(): Unit = {
      stroke(0)
      fill(50)
      ellipse(location.x, location.y, 2 * radius, 2 * radius)
    }
  }
}
