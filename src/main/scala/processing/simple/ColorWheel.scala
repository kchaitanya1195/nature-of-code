package processing.simple

import processing.CustomApplet
import processing.core.PApplet.radians

class ColorWheel extends CustomApplet(200, 200) {
  override def setup(): Unit = {
    background(255)
    smooth()
    noStroke()
  }

  override def draw(): Unit = {
    if (frameCount % 10 == 0) {
      fill(frameCount * 3 % 255, frameCount * 5 % 255, frameCount * 7 % 255)
      pushMatrix()
      translate(100, 100)
      rotate(radians(frameCount * 2 % 360))
      rect(0, 0, 80, 20)
      popMatrix()
    }
  }
}
