package processing.simple

import processing.CustomApplet
import processing.core.PApplet.constrain
import processing.core.PVector

class MutualAttraction extends CustomApplet {
  var movers: Seq[Mover] = _
  var attractor: Attractor = _
  val G = 1

  override def setup(): Unit = {
    movers = (1 to 5).map(_ => Mover(random(0.5f, 4f)))
    attractor = Attractor(20)
  }

  override def draw(): Unit = {
    background(255)

    movers.foreach { mover =>
      val force = attractor.attract(mover)
      mover.applyForce(force)

      movers.foreach { other =>
        if (mover != other) {
          val mForce = other.attract(mover)
          mover.applyForce(mForce)
        }
      }

      mover.update()
      mover.display()
    }

    attractor.display()
  }

  case class Mover(mass: Float) {
    val radius: Float = mass * 5

    val location = new PVector(random(radius, width - radius), random(radius, height - radius))
    val velocity = new PVector(1, 0)
    val acceleration = new PVector(0, 0)

    def update(): Unit = {
      velocity.add(acceleration)
      location.add(velocity)

      acceleration.mult(0)
    }

    def display(): Unit = {
      stroke(0)
      fill(100, 180)
      ellipse(location.x, location.y, 2 * radius, 2 * radius)
    }

    def applyForce(force: PVector): Unit = {
      val f = PVector.div(force, mass)
      acceleration.add(f)
    }

    def attract(mover: Mover): PVector = {
      val force = PVector.sub(location, mover.location)
      val d = constrain(force.mag(), 5, 25)
      val forceMag = (G * mover.mass * mass) / (d * d)

      force.setMag(forceMag)
    }
  }
  case class Attractor(mass: Float) {
    val location = new PVector(width / 2, height / 2)

    def display(): Unit = {
      stroke(0)
      strokeWeight(2)
      fill(50)
      ellipse(location.x, location.y, mass * 2, mass * 2)
    }

    def attract(mover: Mover): PVector = {
      val force = PVector.sub(location, mover.location)
      val d = constrain(force.mag(), 5, 25)
      val forceMag = (G * mover.mass * mass) / (d * d)

      force.setMag(forceMag)
    }
  }
}
