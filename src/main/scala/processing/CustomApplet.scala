package processing

import processing.core.{PApplet, PConstants}

class CustomApplet(cWidth: Int = 640, cHeight: Int = 360) extends PApplet {
  override def settings(): Unit = size(cWidth, cHeight)

  def init(): Unit = ()

  override def setup(): Unit = init()

  override def mouseClicked(): Unit = {
    init()
    if (!looping) loop()
  }

  override def keyPressed(): Unit = {
    key match {
      case ' ' =>
        if (isLooping) noLoop()
        else loop()
      case PConstants.ENTER | PConstants.RETURN =>
        frameCount += 1
        redraw()
      case _ =>
    }
  }
}
