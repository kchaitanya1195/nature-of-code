package processing.genetic.rockets

import processing.CustomApplet
import processing.core.{PConstants, PFont, PVector}

class SmartRockets extends CustomApplet {
  implicit val applet: CustomApplet = this

  var rocketPool: RocketPool = _
  var target: PVector = _
  val lifeTime: Int = 200
  var font: PFont = _

  override def init(): Unit = {
    val target = new PVector(width / 2, 100)
    val popSize = 50
    val mutationRate = 0.01f

    this.target = target
    rocketPool = RocketPool(target, popSize, mutationRate, lifeTime)
    font = createFont("Courier", 32, true)
  }

  var counter: Int = 0
  var generationCounter: Int = 1
  var bestFitness: Float = 0
  override def draw(): Unit = {
    background(0)

    displayInfo()

    if (rocketPool.finished)
      noLoop()

    noStroke()
    fill(255)
    ellipse(target.x, target.y, 20, 20)

    if (counter < lifeTime) {
      rocketPool.run()
      counter += 1
    } else {
      rocketPool.calcFitness()
      bestFitness = rocketPool.best.fitness
      rocketPool.doSelection()
      rocketPool.doGeneration()

      counter = 0
      generationCounter += 1
    }
  }

  def displayInfo(): Unit = {
    textFont(font)
    textAlign(PConstants.LEFT)
    fill(255)

    textSize(24)
    text(s"Generation#$generationCounter",20,30)

    textSize(15)
    text(s"Best fitness: $bestFitness",20,50)
  }
}
