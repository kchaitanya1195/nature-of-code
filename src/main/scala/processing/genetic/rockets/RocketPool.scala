package processing.genetic.rockets

import processing.CustomApplet
import processing.core.PVector
import processing.genetic.Population

case class RocketPool(target: PVector, size: Int, mutationRate: Float, rocketLife: Int)
                             (implicit val applet: CustomApplet) extends Population[PVector, Rocket] {
  override var population: Seq[Rocket] =
    (1 to size).map(_ => Rocket(rocketLife))

  override def finished: Boolean = best.fitness >= 1
  def run(): Unit = {
    population.foreach { rocket =>
      rocket.update()
      rocket.display()
    }
  }
}
