package processing.genetic.rockets

import processing.CustomApplet
import processing.core.{PApplet, PVector}
import processing.genetic.{DNA, DNACompanion}

case class Rocket(genes: Seq[PVector])(implicit val applet: CustomApplet) extends DNA[PVector] {
  import applet._

  override protected def companion: DNACompanion[PVector] = Rocket
  override type Target = PVector

  val location: PVector = new PVector(width / 2, height)
  val velocity: PVector = new PVector(0, 0)

  var counter = 0

  def update(): Unit = {
    if (counter < genes.size) {
      val acceleration = genes(counter)
      velocity.add(acceleration)
      location.add(velocity)

      counter += 1
    }
  }

  def display(): Unit = {
    fill(255)
    ellipse(location.x, location.y, 8, 8)
  }

  override def calcFitness(target: PVector): Unit = {
    val score = PVector.dist(location, target)
    fitness = PApplet.pow(1 / score, 2)
  }
}

object Rocket extends DNACompanion[PVector] {
  override type DNAType = Rocket
  private val maxForce: Float = 0.3f

  override def generateRandomGene: PVector = PVector.random2D().mult(maxForce)
  override def createDNA(genes: Seq[PVector])(implicit applet: CustomApplet): Rocket = Rocket(genes)
}
