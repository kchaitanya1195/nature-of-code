package processing.genetic

import processing.CustomApplet
import processing.core.PApplet

trait Population[Gene, Element <: DNA[Gene]] {
  implicit val applet: CustomApplet
  import applet._

  val target: Element#Target
  val size: Int
  val mutationRate: Float

  var population: Seq[Element]
  var matingPool: Seq[Element] = Seq.empty

  def calcFitness(): Unit = population.foreach(e => e.calcFitness(target.asInstanceOf[e.Target]))

  def doSelection(): Unit = {
    matingPool = Seq.empty
    val maxFitness = population.map(_.fitness).max

    population.filter(_.fitness > 0).foreach { member =>
      val fitness = PApplet.map(member.fitness, 0, maxFitness, 0, 100).toInt
      matingPool ++= Seq.fill(fitness)(member)
    }

    if (matingPool.isEmpty) {
      noLoop()
      println("No valid selections")
    }
  }

  def doGeneration(): Unit = {
    if (matingPool.isEmpty)
      return
    population =
      (1 to size).map { _ =>
        val a = random(matingPool.size).toInt
        val b = random(matingPool.size).toInt
        val parentA = matingPool(a)
        val parentB = matingPool(b)

        var child = parentA.crossoverRandom(parentB)
        child = child.mutate(mutationRate)

        child.asInstanceOf[Element]
      }
  }

  def best: Element = population.maxBy(_.fitness)

  def average: Float = population.map(_.fitness).sum / population.size

  def finished: Boolean = best.fitness == 1f
}
