package processing.genetic.words

import processing.CustomApplet
import processing.core.{PApplet, PConstants, PFont}

class RandomWords extends CustomApplet {
  implicit val applet: RandomWords = this

  var population: WordPool = _
  var font: PFont = _

  override def init(): Unit = {
    val target = "Testing genetic algorithm"
    val popSize = 200
    val mutationRate = 0.01f

    population = WordPool(target, popSize, mutationRate)
    font = createFont("Courier", 32, true)
  }
  override def draw(): Unit = {
    population.calcFitness()

    displayInfo()

    if (population.finished)
      noLoop()

    population.doSelection()
    population.doGeneration()
  }

  def displayInfo() {
    background(0)
    textFont(font)
    textAlign(PConstants.LEFT)
    fill(255)


    textSize(24)
    text("Best phrase:",20,30)
    textSize(40)
    text(population.best.phrase, 20, 100)

    textSize(18)
    text(s"total generations:     $frameCount", 20, 160)
    text(s"average fitness:       ${PApplet.nf(population.average, 0, 2)}", 20, 180)
    text(s"total population:      ${population.size}", 20, 200)
    text(s"mutation rate:         ${population.mutationRate * 100}%", 20, 220)

    textSize(10)
    text(s"All phrases:\n${population.phrases}", 500, 10)
  }
}
