package processing.genetic.words

import processing.CustomApplet
import processing.genetic.{DNA, DNACompanion}

case class Word(genes: Seq[Char])(implicit val applet: CustomApplet) extends DNA[Char]{
  def phrase: String = genes.mkString

  def calcFitness(target: Seq[Char]): Unit = {
    val score = genes.zip(target).count(g => g._1 == g._2).toFloat
    fitness = score / target.length
  }

  override def toString: String = {
    s"Word($phrase, $fitness)"
  }

  override protected def companion: DNACompanion[Char] = Word
  override type Target = Seq[Char]
}

object Word extends CustomApplet with DNACompanion[Char] {
  override type DNAType = Word
  val allowedChars: Seq[Char] =
    ('a' to 'z') ++ ('A' to 'Z') ++ Seq(' ')

  override def generateRandomGene: Char = {
    val i = random(allowedChars.size).toInt
    allowedChars(i)
  }
  override def createDNA(genes: Seq[Char])(implicit applet: CustomApplet): Word = new Word(genes)
}
