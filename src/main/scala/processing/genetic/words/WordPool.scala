package processing.genetic.words

import processing.CustomApplet
import processing.genetic.Population

case class WordPool(target: Seq[Char], size: Int, mutationRate: Float)
                   (implicit val applet: CustomApplet) extends Population[Char, Word] {
  var population: Seq[Word] =
    (1 to size).map(_ => Word(target.size))

  override def finished: Boolean = best.fitness == 1f
  def phrases: String = population.take(50).map(_.phrase).mkString("\n")
}
