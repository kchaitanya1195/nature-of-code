package processing.genetic

import processing.CustomApplet

trait DNA[Gene] {
  type Target

  implicit val applet: CustomApplet

  import applet._

  val genes: Seq[Gene]
  var fitness: Float = 0f

  protected def companion: DNACompanion[Gene]

  def calcFitness(target: Target): Unit

  def crossoverHalf(other: DNA[Gene]): DNA[Gene] = {
    val midpoint = random(genes.size).toInt
    val newGenes = genes.take(midpoint) ++ other.genes.drop(midpoint)

    companion.createDNA(newGenes)
  }

  def crossoverRandom(other: DNA[Gene]): DNA[Gene] = {
    val thisIsHigher = this.fitness > other.fitness
    val (higher, lower) = {
      if (thisIsHigher) (this, other)
      else (other, this)
    }
    val newGenes =
      lower.genes.zip(higher.genes).map { case (lowerGene, higherGene) =>
        val coin = random(1)
        if (coin < 0.1) lowerGene
        else higherGene
      }

    companion.createDNA(newGenes)
  }

  def mutate(mutationRate: Float): DNA[Gene] = {
    val newGenes = genes.map { g =>
      val r = random(1)
      if (r < mutationRate) companion.generateRandomGene
      else g
    }

    companion.createDNA(newGenes)
  }
}

trait DNACompanion[Gene] {
  type DNAType <: DNA[Gene]

  def generateRandomGene: Gene
  def createDNA(genes: Seq[Gene])(implicit applet: CustomApplet): DNAType
  def apply(length: Int)(implicit applet: CustomApplet): DNAType = {
    val genes: Seq[Gene] = (1 to length).map(_ => generateRandomGene)
    createDNA(genes)
  }
}

