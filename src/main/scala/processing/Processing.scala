package processing

import processing.core.PApplet

object Processing extends App {
  PApplet.main("processing.genetic.rockets.SmartRockets")
}
